import { Component, OnInit } from '@angular/core';
import { Company } from 'src/app/models/company';
import { CompanyService } from 'src/app/services/company.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css'],
})
export class CompanyComponent implements OnInit {
  //1. create a variable to display data at UI
  companies: Company[] = [];

  //2. DI for service
  constructor(private service: CompanyService) {}

  //4. on loading component call getAllCompanies
  ngOnInit(): void {
    this.getAllCompanies();
  }

  //3. make service call and fill data in companies array
  getAllCompanies() {
    this.service.getAllCompanies().subscribe({
      next: (data) => {
        this.companies = data;
      },
      error: (e) => {
        console.log(e);
      },
    });
  }
}
