import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

customers:Customer[]=[];

  constructor(private service:CustomerService) { }

  ngOnInit(): void {
    this.getAllCustomers();
  }

  getAllCustomers(){
   this.service.getAllCustomer().subscribe(
     { next:(data)=>{
        this.customers=data;
      },
      error:(error)=>{
        console.log(error);
      }
     });
  }

}
