import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.css']
})
export class CustomerCreateComponent implements OnInit {

  customer: Customer = new Customer(0, '', '', '', '', '', '', '', '');
  message: string | undefined;

  constructor(private service: CustomerService) { }

  ngOnInit(): void {
  }

  createCustomer() {
    this.service.createCustomer(this.customer).subscribe(
      {
        next: (data) => {
          this.message = data;
          this.customer = new Customer(0, '', '', '', '', '', '', '', '');

        },

        error: (e) => {
          alert(e);

        }

      }
    )
  }



}
