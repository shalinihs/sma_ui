import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyComponent } from './components/company/company.component';
import { CustomerCreateComponent } from './components/customer-create/customer-create.component';
import { CustomerComponent } from './components/customer/customer.component';

const routes: Routes = [
  { path: 'company', component: CompanyComponent },
  {path:'customer',component:CustomerComponent},
  {path:'customerCreate',component:CustomerCreateComponent},
  { path: '', pathMatch: 'full', component: CompanyComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
