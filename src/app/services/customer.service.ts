import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Customer } from '../models/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customerUri=`${environment.baseUri}/customer`

  constructor(private http:HttpClient) { }

  getAllCustomer():Observable<Customer[]>{
    return this.http.get<Customer[]>(`${this.customerUri}/all`);
  }
  createCustomer(customer:Customer):Observable<any>{
    return this.http.post(`${this.customerUri}/create`,customer,{responseType:'text'});
  }

  
}

